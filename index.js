"use strict"

//  1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків з довжиною більше за 3 символи.Вивести це число в консоль.

// const arr = ["travel", "hello", "eat", "ski", "lift"];

// const arrSort = arr.filter((str) => str.length > 3).sort();
// console.log(arrSort)



//  2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}. Наповніть різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".
//  Відфільтрований масив виведіть в консоль.

const arr = [
    {
        name: "Іван",
        age: 30,
        sex: "чоловіча"
    },
    {
        name: "Ірина",
        age: 33,
        sex: "жіноча"
    },
    {
        name: "Світлана",
        age: 40,
        sex: "жіноча"
    },
    , {
        name: "Alex",
        age: 40,
        sex: "чоловіча"
    }
]

const filteredBysex = arr.filter((elem) => elem.sex === "чоловіча");
const filteredByAge = arr.filter((elem) => elem.age !== 40);
console.log(filteredBysex);
console.log(filteredByAge);


//  3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)

// Технічні вимоги:

// - Написати функцію filterBy(), яка прийматиме 2 аргументи.Перший аргумент - масив, який міститиме будь - які дані, другий аргумент - тип даних.
// - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом.Тобто якщо передати масив['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив[23, null].
//  * /

function filterBy(arr, dataType) {
    return arr.filter(elem => typeof elem !==dataType )
};

const array = ['hello', 'world', 23, '23', null];
const filteredArray = filterBy(array, "string");
console.log(filteredArray);